import pandas as pd
import json
import requests
import os
from decouple import config
from datetime import datetime as dt

def policy_collector():
    user = config("HX_API_USER")
    password = config("HX_API_KEY")

    # get all policy ids relating to a model id
    url =f"https://api.optio.hxrenew.com/api/v1/policies/options/"

    # W&I Model Number
    model_id = 9


    # Unpaged True to get all policies
    params = {
        "model_id": model_id,
        "unpaged":True
    }

    # Call to API to get policy IDs
    try:
        response = requests.get(url, params=params, auth=(user, password))
    except requests.RequestException:
        print("Unable to connect to API")

    # Error handling based on status code returned by API
    if response.ok:
        # results stored in dict : {data:{variable_name: value}}
        result = response.json()
        policy_ids = []
        for policy in result:
            policy_ids.append(policy["id"])

    list_of_policy_jsons = []

    # For each policy id get its snapshot
    for policy_id in policy_ids:

        # URL & parameters for the API
        url = f"https://api.optio.hxrenew.com/api/v1/policies/options/{policy_id}/snapshot"
        params = {
            "path": [
                "/programme",
                "/policy",
                "/pricing",
                "/risk_assessment",
                "/hx_core",
            ]
        }

        # Call to API
        try:
            response = requests.get(url, params=params, auth=(user, password))
        except requests.RequestException:
            print("Unable to connect to API")

        # Error handling based on status code returned by API
        if response.ok:
            # results stored in dict : {data:{variable_name: value}}
            result = response.json()
            list_of_policy_jsons.append(result)

    # Save the raw data
    now = dt.now().strftime("%Y-%m-%d_%H-%M-%S")
    parent_dir = os.path.dirname(os.path.abspath(__file__))
    dir_path = os.path.join(parent_dir, "raw_data")
    os.makedirs(dir_path, exist_ok=True)
    file_path = os.path.join(dir_path, f"wi_data_{now}.json")

    with open(file_path, 'w') as f:
        json.dump(list_of_policy_jsons, f)

    return list_of_policy_jsons


def wi_to_excel():

    data =  policy_collector()

    policies_data = []
    programme_data = []
    
    for i, record in enumerate(data):
        pol = record["data"]
        policy_data = {
            "id": record["id"],
            "project_name": pol["policy"]["project_name"],
            "insured_name": pol["policy"]["insured_name"],
            "inception_date": pol["hx_core"]["inception_date"],
            "expiry_date": pol["hx_core"]["expiry_date"],
            "status": pol["policy"]["status"],
            "broker": pol["policy"]["broker"],
            "underwriter": pol["policy"]["underwriter"],
            "quoting_currency": pol["policy"]["quoting_currency"],
            "fx_rate": pol["policy"]["fx_rate"],
            "domicile": pol["policy"]["domicile"],
            "governing_law": pol["policy"]["governing_law"],
            "industry_sector": pol["policy"]["industry_sector"],
            "policy_cover_type": pol["policy"]["policy_cover_type"],
            "scope_of_cover": pol["policy"]["scope_of_cover"],
            "type_of_sale": pol["policy"]["type_of_sale"],
            "closing": pol["policy"]["closing"],
            "recourse": pol["policy"]["recourse"],
            "enterprise_value": pol["policy"]["exposure_base_value"],
            "potential_severity": pol["policy"]["potential_severity"],

            # pricing
            "base_premium_gbp": pol["pricing"]["ra_base_premium_gbp"],
            "base_premium_qc": pol["pricing"]["ra_base_premium_qc"],
            "base_rate": pol["pricing"]["base_rate"],

            # risk assessment
            "uw_assessment_1": pol["risk_assessment"]["uw_assessment_1"]["modifier"],
            "uw_assessment_1_comment": pol["risk_assessment"]["uw_assessment_1"]["comment"],
            "uw_assessment_2": pol["risk_assessment"]["uw_assessment_2"]["modifier"],
            "uw_assessment_2_comment": pol["risk_assessment"]["uw_assessment_2"]["comment"],
            "uw_assessment_3": pol["risk_assessment"]["uw_assessment_3"]["modifier"],
            "uw_assessment_3_comment": pol["risk_assessment"]["uw_assessment_3"]["comment"],
            "uw_assessment_total": pol["risk_assessment"]["risk_assessment_total"]
        }

        for layer in pol["programme"]["layers"]:
            pol_layer = {
                "id": record["id"],
                "project_name": pol["policy"]["project_name"],
                "policy_reference": layer["policy_reference"],
                "limit": layer["limit"],
                "excess": layer["excess"],
                "deminimis": layer["deminimis"],
                "brokerage": layer["brokerage"],
                "agreed_gross_premium": layer["agreed_gross_premium"],
                "share": layer["share"],
                "floor": layer["floor"],
                "ceiling": layer["ceiling"],
                "mechanical_gross_premium": layer["mechanical_gross_premium"],
                "mechanical_rol": layer["mechanical_rol"],
                "technical_gross_premium": layer["technical_gross_premium"],
                "technical_rol": layer["technical_rol"],
                "agreed_rol": layer["agreed_rol"],
                "priced_loss_ratio": layer["priced_loss_ratio"],
                "return_on_capital": layer["return_on_capital"],
                "adequacy_to_mechanical_premium": layer["adequacy_to_mechanical_premium"],
                "adequacy_to_technical_premium": layer["adequacy_to_technical_premium"],
            }
            programme_data.append(pol_layer)
        policies_data.append(policy_data)
        

    # Assign to dataframe for writing to excel
    pol_df = pd.DataFrame(policies_data)
    prog_df = pd.DataFrame(programme_data)
    now = dt.now().strftime("%Y-%m-%d_%H-%M-%S")
    with pd.ExcelWriter(f'excel/wi_data_{now}.xlsx') as writer:

        pol_df.to_excel(writer, sheet_name="policy_data", index=False)
        prog_df.to_excel(writer, sheet_name="programme", index=False)


wi_to_excel()